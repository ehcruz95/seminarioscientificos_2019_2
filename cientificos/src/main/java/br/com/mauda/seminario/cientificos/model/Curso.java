package br.com.mauda.seminario.cientificos.model;

public class Curso {

    private Long id;
    private String nome;
    private AreaCientifica areaCientifica;

    public Curso(AreaCientifica areaCientifica) {
        this.areaCientifica = areaCientifica;
        this.areaCientifica.adicionarCurso(this);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public AreaCientifica getAreaCientifica() {
        return this.areaCientifica;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.areaCientifica.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Curso other = (Curso) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        if (this.nome == null) {
            if (other.nome != null) {
                return false;
            }
        } else if (!this.nome.equals(other.nome)) {
            return false;
        }
        return true;
    }

}
